#include <algorithms/nodes/src/node.h>
#include <iostream>

using namespace std;

int main(int argc, char ** argv)
{
    //test for tree node
    TreeNode * root = new TreeNode();
    TreeNode * node = root;
    for(int i = 1; i < 10; i++)
    {
        node -> right = new TreeNode(i);
        node = node -> right;
    }
    while(root)
    {
        cout << root -> val << " ";
        node = root;
        root = root -> right;
        delete node;
    }
    cout << endl;
    
    ListNode * head = new ListNode();
    ListNode * dummy = head;
    for(int i = 1; i < 10; i++)
    {
        dummy -> next = new ListNode(i);
        dummy = dummy -> next;
    }
    while(head)
    {
        cout << head -> val << " ";
        dummy = head;
        head = head -> next;
        delete dummy;
    }
    cout << endl;
}
