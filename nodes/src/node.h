class TreeNode
{
public:
    int val;
    TreeNode * left;
    TreeNode * right;
    TreeNode(): val(0), left(nullptr), right(nullptr){};
    TreeNode(int v): val(v), left(nullptr), right(nullptr){};
};

class ListNode
{
public:
    int val;
    ListNode * prev;
    ListNode * next;
    ListNode(): val(0), prev(nullptr), next(nullptr){};
    ListNode(int v): val(v), prev(nullptr), next(nullptr){};
};
